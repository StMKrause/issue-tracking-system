# Issue-Tracking-System

Simple OpenSource Issue-Tracking-System based on Python/Django. 

## current features
* None

## upcoming features
* Simple, easy to use handling    
    * Browser based interaction
    * Export of the communication
    * Email integration
* high scalable
    * Deployable as Docker container
    * Deployable in a Rancher/Kubernetes cluster

## Licence
